**Setup script for a Debian based linux distro**

The following script will automatically install the following software.

_Note: All rights are reserved by the creators of the software in question_

#### Laptop:

- [Gdb-Dashboard](https://github.com/cyrus-and/gdb-dashboard)
- Perf
- [Nmap](https://github.com/nmap/nmap)
- [Neovim](https://github.com/neovim/neovim) with [Config](https://gitlab.com/SparrowOchon/neovim-setup.git)
- [VsCodium](https://github.com/VSCodium/vscodium)
- [Massif-Visualizer](https://github.com/KDE/massif-visualizer)
- [Valgrind](http://valgrind.org/)
- [Android MTP](https://github.com/whoozle/android-file-transfer-linux)
- [MAT2](https://0xacab.org/jvoisin/mat2)
- [Wine-Staging](https://wiki.winehq.org/Wine-Staging)
- [rr](https://github.com/mozilla/rr)
- [IBM Plex Mono font](https://www.ibm.com/plex/)
- [tmux](https://github.com/tmux/tmux)
- [mkusb](https://github.com/fkmclane/mkusb)
- [Spotify](https://www.spotify.com/)
- [SASM-IDE](https://github.com/Dman95/SASM)
- [IKOS](https://github.com/NASA-SW-VnV/ikos)
- [Conan](https://conan.io/)
- [Redshift](https://github.com/jonls/redshift)
- [taskbook](https://github.com/klaussinani/taskbook)

#### Desktop => Laptop + The following:

- [QT](https://www.qt.io/download/)
- [MXE](https://mxe.cc/#introduction)
- [Makagiga](http://makagiga.sourceforge.net/)
- [Cutter](https://github.com/radareorg/cutter)
- [Radare2](https://github.com/radare/radare2)
- [BinWalk](https://github.com/ReFirmLabs/binwalk)
- [Ghidra](https://ghidra-sre.org/)


#### Needed in Ansible Script:
- Ask and setup Gopath/Goroot
- Run Git Setup script
