#!/bin/bash
#
#	Show don't Sell License Version 1
#   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>
#
# By: Network Silence (30-sept-2018)
#
# The following is a all in one machine setup script for both laptop and desktop.
#
#

#!/bin/bash

BLUE='\033[1;34m'
PURPLE='\033[1;35m'
CYAN='\033[1;36m'
DEF='\033[0m'

PASS=''
install_path='/opt'

if [[ $(/usr/bin/id -u) -eq 0 ]]; then
    echo "Do not run the following Script as Root"
    exit
fi

if [ $# -ne 1 ]; then
  echo 1>&2 "Usage: $0 <Desktop|Laptop>"
  exit
fi

function get_sudoer_pass(){
	while [[ $PASS == "" ]]; do
		sudo -k
		echo -n "Enter the sudo password:"
		read temppass
		echo $temppass | sudo -S echo testpass &> /dev/null
		if ! [ "$(sudo -n echo testpass 2>&1)" == "testpass" ]; then
			echo "Incorrect password was entered"
		else
			PASS=$temppass
		fi
	done
}


get_sudoer_pass
#-----------Prereqs----------------------
echo $PASS | sudo -S apt-get update -y
echo $PASS | sudo -S apt-get dist-upgrade -y
echo $PASS | sudo -S apt-get install build-essential -y
echo $PASS | sudo -S apt-get install git gdb gcc-multilib python3.7 wget software-properties-common -y #Keep it seperate as this is mission critical

echo $PASS | sudo -S apt-get install autoconf automake autopoint bison bzip2 flex g++ g++-multilib gettext \
    intltool libc6-dev-i386 libgdk-pixbuf2.0-dev libltdl-dev libssl-dev libtool-bin \
    make openssl p7zip-full patch pkg-config lzip unzip xz-utils golang-1.13 golang-1.13-go golang-1.13-src\
    cmake libqt4-dev libfuse-dev libreadline-dev libx32atomic1 libx32cilkrts5 \
    libx32gcc1 meld libx32gomp1 libx32itm1 libx32quadmath0 libx32stdc++6 libx32ubsan0 \
    libwebkitgtk-1.0 libgl-dev libncurses5-dev libgtk2.0-dev libatk1.0-dev qt5-default tmux \
    libcairo2-dev libx11-dev libxpm-dev libxt-dev python-dev python-pygments python3-dev python3-pip gperf grc \
    libboost-all-dev libqt5svg5-dev libgmp-dev pandoc llvm libsqlite3-dev linux-perf-$(uname -r | cut -d'.' -f1-2) -y
echo $PASS | sudo -S apt-mark hold python3.*
cd $install_path
#---------------------------------------

#---------------------------Security Related------------------------------------------------
printf "${CYAN}Installing Checkinstall\n"
echo $PASS | sudo -S wget http://ftp.br.debian.org/debian/pool/main/c/checkinstall/checkinstall_1.6.2-4_amd64.deb --wait=2
echo $PASS | sudo -S dpkg -i checkinstall_1.6.2-4_amd64.deb
echo $PASS | sudo -S rm checkinstall_1.6.2-4_amd64.deb

printf "${PURPLE}Installing Nmap\n"
echo $PASS | sudo -S git clone https://github.com/nmap/nmap.git
cd nmap
echo $PASS | sudo -S ./configure
echo $PASS | sudo -S make
echo $PASS | sudo -S checkinstall
cd $install_path

printf "${BLUE}Installing GDB Dashboard\n"
echo $PASS | sudo -S pip3 install --upgrade pip
echo $PASS | wget -P ~ https://git.io/.gdbinit
mv .gdbinit ~/.gdbinit
echo $PASS | sudo -S pip3 install pygments

#-------------------------------------------------------------------------------------------


#---------------------------Development Related------------------------------------------------
printf "${PURPLE}Installing Conan\n"
echo $PASS | sudo -S pip3 install conan
cd $install_path


printf "${BLUE}Installing Sasm\n"
echo $PASS | sudo -S wget http://download.opensuse.org/repositories/home:/Dman95/Debian_9.0/amd64/sasm_3.10.1_amd64.deb --wait=2
echo $PASS | sudo -S apt-get install nasm -y
echo $PASS | sudo -S dpkg -i sasm_3.10.1_amd64.deb
echo $PASS | sudo -S rm sasm_3.10.1_amd64.deb

printf "${CYAN}Installing Valgrind\n"
echo $PASS | sudo -S apt-get install valgrind -y

printf "${PURPLE}Installing VsCodium\n"
echo $PASS | sudo -S wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/usr/share/keyrings/vscodum-archive-keyring.gpg
echo $PASS | sudo -S echo 'deb [signed-by=/usr/share/keyrings/vscodum-archive-keyring.gpg] https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee /etc/apt/sources.list.d/vscodium.list
echo $PASS | sudo -S apt-get update -y
echo $PASS | sudo -S apt-get install codium -y


printf "${CYAN}Installing rr\n"
echo $PASS | sudo -S apt-get install ccache cmake make g++-multilib gdb \
  pkg-config coreutils python3-pexpect manpages-dev git capnproto libcapnp-dev -y

echo $PASS | sudo -S git clone https://github.com/mozilla/rr.git
cd rr
echo $PASS | sudo -S ./configure
echo $PASS | sudo -S make -j8
echo $PASS | sudo -S make install
cd $install_path

printf "${BLUE}Installing Wine\n"
echo $PASS | sudo -S wget -nc https://dl.winehq.org/wine-builds/winehq.key
echo $PASS | sudo -S apt-key add winehq.key
echo $PASS | sudo -S echo "deb https://dl.winehq.org/wine-builds/debian/ buster main" | sudo tee /etc/apt/sources.list.d/wine-staging.list
echo $PASS | sudo -S apt-get update -y
echo $PASS | sudo -S apt-get install --install-recommends winehq-staging -y


printf "${CYAN}Installing IKOS\n"
echo $PASS | sudo -S git clone https://github.com/NASA-SW-VnV/ikos.git
echo $PASS | sudo -S mkdir ikos/build
cd ikos/build
echo $PASS | sudo -S cmake ..
echo $PASS | sudo -S make
echo $PASS | sudo -S make install
echo $PASS | sudo -S echo -e "\nexport PATH=$PATH:/opt/ikos/install/bin" >> ~/.bashrc
cd $install_path


printf "${BLUE}Installing Massif Visualizer\n"
echo $PASS | sudo -S apt-get install massif-visualizer -y


printf "${PURPLE}Installing neovim\n"
echo $PASS | sudo -S git clone https://gitlab.com/SparrowOchon/neovim-setup.git  # neovim config that will be automatically installed
echo $PASS | sudo -S chmod +x neovim-setup/install.sh
cd neovim-setup

#-----------The following assumes you do not want to install QT5-------------------------------
#wget -q https://github.com/radareorg/cutter/releases/download/v1.0/Cutter-x86_64.AppImage
#chmod +x Cutter-x86_64.AppImage
#ln -s ${install_path}/Cutter-x86_64.AppImage /usr/local/bin/cutter
#-----------------------------------------------------------------------------------------------

if [[ "$1" == "Desktop" ]];then

	#Finish Installing Full vim config
	echo $PASS | ./install.sh full
	cd $install_path

        printf "${BLUE}Installing Ghidra"
        echo $PASS | sudo -S wget https://ghidra-sre.org/ghidra_9.1.2_PUBLIC_20200212.zip
        echo $PASS | sudo -S chmod +x ghidra_9.1_PUBLIC/ghidraRun


        printf "${PURPLE}Installing BinWalk\n"
        echo $PASS | sudo -S apt-get install binwalk -y


	printf "${PURPLE}Installing Radare2\n"
	echo $PASS | sudo -S git clone https://github.com/radare/radare2.git
	echo $PASS | sudo -S chmod +x radare2/sys/install.sh
	cd radare2/sys
	echo $PASS | sudo -S install.sh
	cd $install_path

	printf "${BLUE}Installing QT5\n" #Needed for Cutter
	echo $PASS | sudo -S wget http://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run
	echo $PASS | sudo -S chmod +x qt-unified-linux-x64-online.run
	./qt-unified-linux-x64-online.run
	echo $PASS | sudo -S export QT_SELECT=qt5
	echo $PASS | sudo -S git clone https://github.com/radareorg/cutter.git
	echo $PASS | sudo -S mkdir cutter/build
	echo $PASS | sudo -S cutter/build.sh
	cd $install_path
	echo $PASS | sudo -S ln -s ${install_path}/cutter/build/cutter /usr/local/bin/cutter
	echo $PASS | sudo -S rm qt-unified-linux-x64-online.run


	printf "${CYAN}Installing MXE\n"
	echo $PASS | sudo -S git clone https://github.com/mxe/mxe.git
	cd mxe
	#make cc #Might have to run this first to skip Aubio install (https://github.com/aubio/aubio/issues/173)
	echo $PASS | sudo -S make MXE_TARGETS='x86_64-w64-mingw32.static i686-w64-mingw32.static' qtbase #Cross-Compile 64bit windows Applications
	echo $PASS | sudo -S echo -e "\nexport PATH=$PATH:/opt/mxe/usr/bin" >> ~/.bashrc
	cd $install_path

	printf "${PURPLE}Installing Makagiga\n"
	echo $PASS | sudo -S wget https://downloads.sourceforge.net/project/makagiga/Makagiga%205.x/5.8.4/makagiga_5.8.4-1_all.deb?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fmakagiga%2Ffiles%2FMakagiga%25205.x%2F5.8.4%2Fmakagiga_5.8.4-1_all.deb%2Fdownload&ts=1556251831 --wait=6
	echo $PASS | sudo -S dpkg -i makagiga_5.8.4-1_all.deb
else
	#Neovim for only compiled languages
	echo $PASS | ./install.sh compiled
	cd $install_path
fi


#--------------------------Other/General purpose-------------------------------------
#Install Android MTP
printf "${BLUE}Installing Android MTP\n"
echo $PASS | sudo -S git clone https://github.com/whoozle/android-file-transfer-linux.git
echo $PASS | sudo -S mkdir android-file-transfer-linux/build
cd android-file-transfer-linux/build
echo $PASS | sudo -S cmake ..
echo $PASS | sudo -S make
cd $install_path

printf "${CYAN}Installing IBM Plex Mono Font\n"
echo $PASS | sudo -S wget https://github.com/IBM/plex/releases/download/v1.4.1/TrueType.zip
echo $PASS | sudo -S unzip TrueType.zip
echo $PASS | sudo -S rm TrueType.zip
echo $PASS | sudo -S mkdir /usr/local/share/fonts/ms_fonts
echo $PASS | sudo -S mv TrueType/IBM-Plex-Mono/*.ttf /usr/local/share/fonts/ms_fonts
echo $PASS | sudo -S mv TrueType/IBM-Plex-Sans/*.ttf /usr/local/share/fonts/ms_fonts
echo $PASS | sudo -S mv TrueType/IBM-Plex-Serif/*.ttf /usr/local/share/fonts/ms_fonts
echo $PASS | sudo -S rm -R TrueType
echo $PASS | sudo -S chown root:staff /usr/local/share/fonts/ms_fonts -R
echo $PASS | sudo -S chmod 644 /usr/local/share/fonts/ms_fonts/* -R
echo $PASS | sudo -S chmod 755 /usr/local/share/fonts/ms_fonts
echo $PASS | sudo -S fc-cache -fv

printf "${PURPLE}Installing Tmux\n"
echo $PASS | sudo -S apt-get install tmux -y

printf "${BLUE}Installing mkusb\n"
echo $PASS | sudo -S apt-get install dirmngr
echo $PASS | sudo -S apt-key adv --keyserver keyserver.ubuntu.com --recv 54B8C8AC
echo $PASS | sudo -S add-apt-repository ppa:mkusb/ppa -y
echo $PASS | sudo -S apt-get update -y
echo $PASS | sudo -S apt-get install mkusb mkusb-nox guidus dus usb-pack-efi mkusb-common -y

printf "${CYAN}Installing MAT2\n"
echo $PASS | sudo -S apt-get install mat2 -y

printf "${PURPLE}Installing Spotify\n"
echo $PASS | sudo -S apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 931FF8E79F0876134EDDBDCCA87FF9DF48BF1C90
echo $PASS | sudo -S echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
echo $PASS | sudo -S apt-get update -y
echo $PASS | sudo -S apt-get install spotify-client -y --allow-unauthenticated

printf "${BLUE}Installing Taskbook\n"
npm install --global taskbook

printf "${CYAN}Installing Redshift\n"
echo $PASS | sudo -S apt-get install redshift gtk-redshift -y

#------------------------------------------------------------------------------------

#----------------------------Configs----------------------------------------
printf "${DEF}"

echo $PASS | sudo -S git clone https://gitlab.com/SparrowOchon/dotfiles.git
if [[ $(echo $XDG_CURRENT_DESKTOP) == "XFCE" ]];then
	cat dotfiles/terminalrc > ~/.config/xfce4/terminal/terminalrc
fi
cat dotfiles/bash_aliases > ~/.bash_aliases
cat dotfiles/tmux.conf > ~/.tmux.conf
cat dotfiles/valgrindrc > ~/.valgrindrc
cat dotfiles/vscodium-settings.json > ~/.config/VsCodium/User/settings.json
echo $PASS | sudo -S mv dotfiles/plugin-configs ~/opt/
echo $PASS | sudo -S rm -R dotfiles
source ~/.bash_aliases
tmux source-file ~/.tmux.conf

# Cleanup
echo $PASS | sudo -S apt-get autoremove -y
echo $PASS | sudo -S apt-get autoclean -y

